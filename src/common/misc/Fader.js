import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes } from "styled-components";

function Fader(props) {
  const { image, width, height } = props;

  return (
    <Container>
      <Image style={{ height, width }} src={image} alt="loading" />
    </Container>
  );
}

Fader.propTypes = {
  height: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
};

const fade = keyframes`
  0% {
    opacity: 1;
  }

  50% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
`;

const Container = styled.div`
  align-items: center;
  display: flex;
  height: 245px;
  justify-content: center;
`;

const Image = styled.img`
  -webkit-animation: ${fade} 2s linear infinite;
  -moz-animation: ${fade} 2s linear infinite;
  animation: ${fade} 2s linear infinite;
`;

export default Fader;
