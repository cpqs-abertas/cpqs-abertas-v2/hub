import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./styles/general.css";

import Home from "./pages/Home";
import Sobre from "./pages/Sobre";
import Inscricao from "./pages/Inscricao";
import Menu from "./components/Menu";
import FAQ from "./components/faq/FAQ";
import { Body } from "./styles";
import Header from "./components/misc/Header";

function App() {
  const [menuFilter, setMenuFilter] = useState({
    home: true,
    sobre: false,
    faq: false,
    inscricao: false,
  });

  useEffect(() => {
    document.querySelector("title").innerText = "Hub CPQs Abertas";
    document.querySelector('link[rel="shortcut icon"]').href =
      "/hub/favicon.ico";
  }, []);

  return (
    <Router>
      <Body>
        <Header />
        <Menu menuFilter={menuFilter} setMenuFilter={setMenuFilter} />
        {menuFilter.faq && <FAQ setMenuFilter={setMenuFilter} />}

        <Switch>
          <Route exact path="/">
            <Home setMenuFilter={setMenuFilter} />
          </Route>
          <Route exact path="/sobre">
            <Sobre setMenuFilter={setMenuFilter} />
          </Route>
          <Route exact path="/inscricao">
            <Inscricao setMenuFilter={setMenuFilter} />
          </Route>
        </Switch>
      </Body>
    </Router>
  );
}

export default App;
