import React, { useEffect } from "react";
import PropTypes from "prop-types";
import CarouselWrapper from "../components/wrappers/CarouselWrapper";
import HubArrow from "../assets/images/Seta FAQ.svg";
import { setFilters } from "../helpers";

function Home(props) {
  const { setMenuFilter } = props;

  useEffect(() => {
    setFilters(setMenuFilter, { home: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  return <CarouselWrapper arrowImage={HubArrow}></CarouselWrapper>;
}

Home.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default Home;
