import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { setFilters } from "../helpers";
import Form from "../components/inscricao/Form";

function Inscricao(props) {
  const { setMenuFilter } = props;

  useEffect(() => {
    setFilters(setMenuFilter, { inscricao: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);
  return (
    <Container>
      <div>
        <h2>Inscrição de Instituto</h2>
        <hr />
        A iniciativa das CPQs abertas foi desenvolvida com a ideia de alcançar o
        maior número possível de institutos, nos mais variados campus, dentro da
        USP.
        <br />
        <br />
        Dessa forma, o projeto está sempre aberto à novas parcerias com os
        institutos interessados. A ordem de desenvolvimento é possível ser vista
        ao lado, e é influenciada pela ordem de inscrição. Assim, para entrar em
        contato conosco, basta preencher o formulário abaixo para que possamos
        começar o processo.
        <br />
        <br />
        Importante notar que, no decorrer do processo, exigiremos alguns
        materiais. Pensando nas nossas necessidades, decidimos que os seguintes
        conteúdos sejam separados:
        <Alert>
          <h3>Aviso</h3>
          <p>Atualmente, a funcionalidade de inscrição por e-mail não está funcionando.</p>
        </Alert>
        <Form postQuestion={() => {}} />
      </div>
      <div />
      <div>
        <h2>Em desenvolvimento</h2>
        <hr />
      </div>
    </Container>
  );
}

Inscricao.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

const Container = styled.div`
  display: grid;
  grid-column-start: 3;
  grid-row-start: 4;

  font-family: "Raleway";

  grid-template-columns: 705px 1fr 350px;
  width: 100%;

  div > h2 {
    font-family: Raleway:wght@700;900;
    font-size: 26px;
    font-weight: 700;
    color: #555555;
  }

  div > hr {
    border: 1px solid;
    border-radius: 60px;
    color: #A6A6A6;
  }
`;

const Alert = styled.div`
  padding: 5px 5px 5px 20px;
  background-color: #ffeb3b;
  color: black;
  margin-top: 20px;
`;

export default Inscricao;
