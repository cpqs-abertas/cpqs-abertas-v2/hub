import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { setFilters } from "../helpers";

function Sobre(props) {
  const { setMenuFilter } = props;

  useEffect(() => {
    setFilters(setMenuFilter, { sobre: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  return (
    <Container>
      <div>
        <h2>Sobre o Projeto</h2>
        <hr />
        A CPQs Abertas é um projeto promovido pela PRPI-USP com o intuito de dar
        visibilidade à produção intelectual da faculdade, difundindo sua
        especificidade e diversidade através de dados extraídos do currículo
        Lattes dos docentes. Como se sabe, é compromisso das universidades
        públicas ampliar o acesso às suas pesquisas acadêmicas e aos seus
        resultados (produções bibliográficas, técnicas e artísticas), permitindo
        quantificá-los e qualificá-los em termos de impacto social, impacto
        econômico, inovação tecnológica e desdobramentos em políticas públicas e
        de sustentabilidade.
        <br />
        <br />
        O projeto já passou por quatro etapas, contando com a participação de
        diversos docentes e discentes da FAU e do IME, todos devidamente
        creditados ao lado. Tendo seu inicio no FAUaberta, a iniciativa hoje já
        possui também sites para a FEA-RP e para o IME, disponibilizando os
        dados lattes produzidos nas mesmas, além da produção dos docentes.
        Seguimos contando com o apoio do STI, da CPQ e da direção das unidades,
        e planejamos expandir para outros institutos em um futuro próximo.
        <br />
        <br />
        Para explorar os sites já disponíveis, basta entrar na sessão
        CPQs-Abertas e clicar nas imagens disponíveis no carrossel. Caso tenha
        interesse em inscrever sua unidade no projeto, basta entrar em contato
        na seção Inscrição de instituto, a qual contém também informações sobre
        o que será requisitado para o prosseguimento do processo.
      </div>
      <div />
      <div>
        <h2>Equipe</h2>
        <hr />
        <b>coordenação do projeto CPqs Abertas:</b> Alfredo Goldman vel Lejbman
        <br />
        <br />
        <b>pesquisadores colaboradores:</b> Artur Rozestraten, Beatriz Bueno,
        Leandro Velloso, Amarílis Corrêa, Harley Macedo e Deidson Rafael
        Trindade
        <br />
        <br />
        <b>etapa I, 2019 | desenvolvimento:</b> César Fernandes, Leonardo
        Aguilar, Larissa Sala, Mateus dos Anjos, Matheus Cunha, Nathalia Borin,
        Pedro Santos, Victor Batistella
        <br />
        <br />
        <b>etapa II, 2020 | desenvolvimento:</b> Kaique Komata, Jean Pereira,
        Luciana Marques, Priscila Lima | <b>design:</b> Luís Felipe Abbud
        <br />
        <br />
        <b>etapa III, 2021 | desenvolvimento:</b> João Daniel, João Gabriel
        Lembo, Leonardo Pereira, Victor Lima | <b>design:</b> Gustavo Machado.{" "}
        <b>coordenação:</b>
        Luís Felipe Abbud
        <br />
        <br />
        <b>etapa IV, 2022 | desenvolvimento:</b> João Daniel, Rafael Rodrigues,
        Mohamad Rkein | <b>design:</b> Gustavo Machado. <b>coordenação:</b> Luís
        Felipe Abbud
      </div>
    </Container>
  );
}

Sobre.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

const Container = styled.div`
  display: grid;
  grid-column-start: 3;
  grid-row-start: 4;

  font-family: "Raleway";

  grid-template-columns: 705px 1fr 350px;
  width: 100%;

  div > h2 {
    font-family: Raleway:wght@700;900;
    font-size: 26px;
    font-weight: 700;
    color: #555555;
  }

  div > hr {
    border: 1px solid;
    border-radius: 60px;
    color: #A6A6A6;
  }
`;

export default Sobre;
