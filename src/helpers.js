const setFilters = (setState, filtersToIsActiveMap, handleFilter) => {
  setState((prevState) => {
    const newState = {};
    Object.keys(prevState).forEach((filter) => {
      handleFilter(filter, newState, prevState);
    });

    Object.keys(filtersToIsActiveMap).forEach((filter) => {
      newState[filter] = filtersToIsActiveMap[filter];
    });

    return newState;
  });
};

export default setFilters;
export { setFilters };
