import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes } from "styled-components";

function Spinner(props) {
  const { image, width, height } = props;

  return (
    <Container>
      <Image style={{ height, width }} src={image} alt="loading" />
    </Container>
  );
}

Spinner.propTypes = {
  height: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
};

const spin = keyframes`
  100% {
    transform: rotate(360deg);
  }
`;

const Container = styled.div`
  align-items: center;
  display: flex;
  height: 245px;
  justify-content: center;
`;

const Image = styled.img`
  -webkit-animation: ${spin} 7s linear infinite;
  -moz-animation: ${spin} 7s linear infinite;
  animation: ${spin} 7s linear infinite;
`;

export default Spinner;
