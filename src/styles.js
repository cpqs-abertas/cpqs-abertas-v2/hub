import styled from "styled-components";

const highlightsFontFamily = "ITC Avant Garde Gothic Std";
const secondaryFontFamily = "DTL Prokyon";

const instituteColor = "#008DC8";
const supportColor = "#555555";

const CenteredColumn = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  grid-column-start: 3;
  grid-row-start: 4;
  overflow-y: auto;
  height: 100%;
  justify-content: center;
`;

const Body = styled.div`
  display: grid;
  font-family: ${secondaryFontFamily};
  grid-template-columns: minmax(25px, 1fr) 230px 1100px minmax(25px, 1fr);
  grid-template-rows: 20px 107px 60px 600px;
  padding-right: 100px;
  height: 99.1vh;
  scrollbar-color: #fff #000;
  width: 100%;

  background: #ffffff 0% 0% no-repeat padding-box;
  opacity: 1;

  button,
  datalist,
  fieldset,
  input,
  label,
  legend,
  output,
  option,
  optgroup,
  select,
  textarea {
    font-family: ${secondaryFontFamily};
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
`;

const Conteudo = styled.div`
  grid-column-start: 3;
  grid-row-start: 4;
`;

const DivCard = styled.div`
  background-color: #fff;
  border-color: ${(props) => props.borderColor || supportColor};
  border-radius: 4px;
  border-style: solid;
  border-width: ${(props) => (props.borderColor ? "5px" : "2px")};
  display: grid;
  grid-template-columns 550px auto;
  grid-template-rows 40% 60%;
  height: 100%;
  position: relative;
  width: 100%;
`;

const DivInfo = styled.div`
  display: grid;
  grid-column-end: 2;
  grid-row-start: 2;
  grid-template-columns 193px 1fr;
  height: 100%;
`;

const DivTotal = styled.div`
  display: flex;
  flex-flow: column;
  font-family: ${highlightsFontFamily};
  font-weight: bold;
  margin-top: 15px;
  width: 100%;

  > * {
    font-size: 25px;
  }

  > *:first-child {
    color: ${instituteColor};
    margin-bottom: -17px;
  }

  > *:last-child {
    color: ${supportColor};
  }
`;

const DivTitle = styled.div`
  color: ${supportColor};
  font-family: ${highlightsFontFamily};
  font-weight: bold;
  width: 90%;

  > span {
    font-size: 32px;
    font-weight: bold;
  }
`;

const DivInfoText = styled.div`
  display: flex;
  flex-direction: column;
  grid-column-start: 2;
  grid-row-end: 3;
  grid-row-start: 2;
  overflow-x: hidden;
  padding: 20px;

  > .title {
    font-family: ${highlightsFontFamily};
  }

  > span {
    :nth-child(2) {
      margin-top: -10px;
    }

    font-size: 13.5px;
    line-height: 1.5;
    margin-bottom: 10px;
  }
`;

const DivLink = styled.div`
  > div {
    display: flex;
    flex-direction: column;
    flex-overflow: column;

    > span#ano {
      font-size: 25px;
      font-weight: bold;
    }

    div > span {
      margin-bottom: 5px;
    }
  }
`;

const DivDetailBlock = styled.div`
  a {
    color: ${(props) => (props.color ? props.color : "#000")};
    font-size: 15px;
    font-weight: bold;
    text-decoration: none;
  }
`;

export {
  highlightsFontFamily,
  instituteColor,
  supportColor,
  Body,
  CenteredColumn,
  Conteudo,
  DivCard,
  DivDetailBlock,
  DivInfo,
  DivInfoText,
  DivLink,
  DivTitle,
  DivTotal,
};
