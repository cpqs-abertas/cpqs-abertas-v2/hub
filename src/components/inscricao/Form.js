import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { instituteColor, supportColor } from "../../styles";

function Form(props) {
  const { postQuestion } = props;
  const [isShowingEmailSentMessage, setIsShowingEmailSentMessage] =
    useState(false);
  const [isShowingErrorMessage, setIsShowingErrorMessage] = useState(false);
  const [hasEmailError, setHasEmailError] = useState(false);
  const [hasTelefoneError, setHasTelefoneError] = useState(false);
  const [hasInstituteError, setHasInstituteError] = useState(false);
  const [hasRelationError, setHasRelationError] = useState(false);

  const handleFormSubmit = (event) => {
    event.preventDefault();
    const form = event.target;
    const params = {};
    for (let i = 0; i < form.elements.length; i++) {
      const element = form.elements[i];
      if (element.name) {
        params[element.name] = element.value;
      }
    }

    let hasError = false;
    const emailRegex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegex.test(form.email.value)) {
      hasError = true;
      setHasEmailError(true);
      setTimeout(() => {
        setHasEmailError(false);
      }, 3000);
    }

    if (!form.telefone.value) {
      hasError = true;
      setHasTelefoneError(true);
      setTimeout(() => {
        setHasTelefoneError(false);
      }, 3000);
    }

    if (!form.institute.value) {
      hasError = true;
      setHasInstituteError(true);
      setTimeout(() => {
        setHasInstituteError(false);
      }, 3000);
    }

    if (!form.relation.value) {
      hasError = true;
      setHasRelationError(true);
      setTimeout(() => {
        setHasRelationError(false);
      }, 3000);
    }

    if (hasError) {
      return;
    }

    postQuestion(params)
      .then(() => {
        setIsShowingEmailSentMessage(true);
        setTimeout(() => {
          setIsShowingEmailSentMessage(false);
        }, 3000);
      })
      .catch(() => {
        setIsShowingErrorMessage(true);
        setTimeout(() => {
          setIsShowingErrorMessage(false);
        }, 3000);
      });
  };

  let hasError = false;
  if (
    hasEmailError ||
    hasTelefoneError ||
    hasInstituteError ||
    hasRelationError
  ) {
    hasError = true;
  }

  return (
    <form style={{ marginTop: "2rem" }} onSubmit={handleFormSubmit}>
      <div style={{ marginBottom: "0.25rem" }}>
        <FormInput name="name" id="name" placeholder="Nome completo" />
      </div>
      <div style={{ marginBottom: "0.25rem", display: "flex" }}>
        <FormInput
          hasError={hasEmailError}
          name="email"
          id="email"
          placeholder="E-mail"
          style={{ marginRight: "0.50rem" }}
        />
        <FormInput
          hasError={hasTelefoneError}
          name="telefone"
          id="telefone"
          placeholder="Telefone"
        />
      </div>
      <div style={{ marginBottom: "0.25rem" }}>
        <FormInput
          hasError={hasInstituteError}
          name="institute"
          id="institute"
          placeholder="Instituto interessado"
        />
      </div>
      <div style={{ marginBottom: "0.25rem" }}>
        <FormInput
          hasError={hasRelationError}
          name="relation"
          id="relation"
          placeholder="Relação com instituto"
        />
      </div>

      <FormSubmitButton type="submit" style={{ fontFamily: "Raleway" }}>
        Enviar
      </FormSubmitButton>

      {isShowingEmailSentMessage && (
        <div style={{ marginTop: "1rem", textAlign: "center" }}>
          E-mail enviado com sucesso!
        </div>
      )}

      {isShowingErrorMessage && (
        <FormErrorArea>
          Erro no envio, tente novamente mais tarde!
        </FormErrorArea>
      )}

      {hasError && (
        <FormErrorArea>
          Preencha todos os campos e/ou verifique a formatação!
        </FormErrorArea>
      )}
    </form>
  );
}

Form.propTypes = {
  postQuestion: PropTypes.func.isRequired,
};

const FormInput = styled.input`
  font-size: 20px;
  border-width: 1px;
  border-style: solid;
  border-color: ${(props) => (props.hasError ? "#FF0000" : supportColor)};
  padding-left: 0.25rem;
  width: 100%;
  height: 30px;
  &::placeholder {
    padding: 15px;
  }
`;

const FormSubmitButton = styled.button`
  background: #272727;
  border: 0;
  color: #ffffff;
  cursor: pointer;
  font-weight: 700;
  font-size: 16px;
  padding: 0.4rem 1.5rem;
  margin-top: 10px;

  :active {
    background: ${instituteColor};
  }
`;

const FormErrorArea = styled.div`
  color: #ff0000;
  font-weight: bold;
  margin-top: 1rem;
  text-align: center;
`;

export default Form;
