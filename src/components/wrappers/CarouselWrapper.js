import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Card from "../misc/Card";
import { cpqs } from "../../assets/texts/cards";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { useState } from "react";

function CarouselWrapper(props) {
  const { arrowImage } = props;
  const [currentIndex, setIndex] = useState(1);

  return (
    <CarouselContainer>
      <CarouselButtonWrapper>
        <CarouselButton
          style={{ position: 0 }}
          onClick={() => setIndex(currentIndex - 1)}
          hidden={currentIndex < 2}
        >
          <img
            style={{ transform: "rotate(180deg)" }}
            width="15"
            height="24"
            src={arrowImage}
            alt="slider-left"
          />
        </CarouselButton>
      </CarouselButtonWrapper>
      <div>
        <Carousel
          showArrows={false}
          showThumbs={false}
          showIndicators={false}
          showStatus={false}
          autoPlays
          centerMode
          selectedItem={currentIndex}
          centerSlidePercentage={100 / 3}
          interval={5000}
        >
          {cpqs.map((cpq) => (
            <Card
              name={cpq.nome}
              institute={cpq.sigla}
              year={cpq.ano}
              description={cpq.descricao}
              color={cpq.cor}
              logo={cpq.logo}
              logoWidth={cpq.logoWidth}
              logoHeight={cpq.logoHeight}
              image={cpq.imagem}
              link={cpq.link}
              available={cpq.disponivel}
            />
          ))}
        </Carousel>
      </div>
      <CarouselButtonWrapper>
        <CarouselButton
          style={{ position: 0 }}
          onClick={() => setIndex(currentIndex + 1)}
          hidden={currentIndex > cpqs.length - 3}
        >
          <img width="15" height="24" src={arrowImage} alt="slider-right" />
        </CarouselButton>
      </CarouselButtonWrapper>
    </CarouselContainer>
  );
}

const CarouselContainer = styled.div`
  display: grid;
  height: 600px;
  width: 1100px;
  grid-column-start: 3;
  grid-row-start: 4;

  grid-template-columns: 15px 1fr 15px;
`;

CarouselWrapper.propTypes = {
  arrowImage: PropTypes.string.isRequired,
};

const CarouselButton = styled.button`
  background: none;
  border: 0;
  margin-top: -220px;
  color: #000000;
  cursor: pointer;
  font-size: 26px;
  padding: 0;
  opacity: 1;
  transition: all 0.25s ease-in;
`;

const CarouselButtonWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export default CarouselWrapper;
