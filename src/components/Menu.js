/* eslint-disable no-unused-vars */
import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { setFilters } from "../helpers";

function Menu(props) {
  const { menuFilter, setMenuFilter } = props;

  const styles = {
    selected: {
      backgroundColor: "#555555",
      borderColor: "transparent",
      color: "#FFFFFF",
    },
    unselected: {},
  };

  const menuStyles = {
    home: "unselected",
    sobre: "unselected",
    faq: "unselected",
    inscricao: "unselected",
  };

  Object.keys(menuFilter).forEach((filter) => {
    if (menuFilter[filter]) {
      menuStyles[filter] = "selected";
    }
  });

  const faqStyle = {
    ...styles[menuStyles.faq],
    fontFamily: "Raleway",
  };

  return (
    <Container id="links">
      <MenuLink to="/" style={styles[menuStyles.home]}>
        CPQs Abertas
      </MenuLink>

      <hr />

      <MenuLink to="/sobre" style={styles[menuStyles.sobre]}>
        Sobre o Projeto
      </MenuLink>

      <MenuButton
        onClick={() => {
          setFilters(
            setMenuFilter,
            { faq: true },
            (filter, newState, prevState) => {
              newState[filter] = prevState[filter];
            }
          );
        }}
        style={faqStyle}
      >
        FAQ
      </MenuButton>

      <MenuLink to="/inscricao" style={styles[menuStyles.inscricao]}>
        Inscrição de Instituto
      </MenuLink>
    </Container>
  );
}

Menu.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  grid-column-start: 2;
  grid-row-start: 4;
  justify-content: top;
  margin-top: 75px;

  > a,
  > button {
    font-family: Raleway;
    font-weight: 600;
    align-items: center;
    background-color: #ffffff;
    border: 1px solid #272727;
    color: #272727;
    cursor: pointer;
    display: flex;
    font-size: 15px;
    height: 40px;
    justify-content: space-around;
    text-align: center;
    text-decoration: none;
    width: 200px;
    letter-spacing: 0px;
    opacity: 1;
  }

  > hr {
    border: 1px solid;
    width: 200px;
    border-radius: 60px;
    color: #a6a6a6;
    margin: 0 0 15px 0;
  }

  a:not(:last-child),
  button:not(:last-child) {
    margin-bottom: 15px;
  }
`;

const MenuLink = styled(Link)`
  :hover {
    background: #555555 0% 0% no-repeat padding-box;
    border: 1px solid #272727;
    border-radius: 2px;
    color: #ffffff;
    opacity: 1;
  }
`;

const MenuButton = styled.button`
  :hover {
    background: #555555 0% 0% no-repeat padding-box;
    border: 1px solid #272727;
    border-radius: 2px;
    color: #ffffff;
    opacity: 1;
  }
`;

export default Menu;
