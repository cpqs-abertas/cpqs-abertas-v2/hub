import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

function Card(props) {
  const {
    name,
    institute,
    year,
    description,
    color,
    logo,
    logoWidth,
    logoHeight,
    image,
    link,
    available,
  } = props;
  console.log(logoWidth);
  return (
    <Container>
      <Logo src={logo} logoWidth={logoWidth} logoHeight={logoHeight} />
      {available ? (
        <ClickableImage href={link}>
          <Image src={image} />
        </ClickableImage>
      ) : (
        <ImageContainer>
          <BlurredImage src={image} />
          <BlurredImageCaption>
            <Name>{name}</Name>
            <EmBreve></EmBreve>
          </BlurredImageCaption>
        </ImageContainer>
      )}
      <Institute color={color}>{institute}</Institute>
      <Year>{year}</Year>
      <Description>{description}</Description>
    </Container>
  );
}

function EmBreve() {
  const Span = styled.span`
    font: normal normal italic 15px Raleway;
  `;

  return <Span>Em breve</Span>;
}

Card.propTypes = {
  name: PropTypes.string.isRequired,
  institute: PropTypes.string.isRequired,
  year: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
  logoWidth: PropTypes.string.isRequired,
  logoHeight: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  available: PropTypes.bool.isRequired,
};

const Container = styled.div`
  height: 100%;
  padding: 20px 20px 0 0;
  justify-content: left;
  text-align: left;
  margin: 0;
`;
const Logo = styled.img`
  object-fit: contain;
  height: 60px;
  max-width: ${(props) => props.logoWidth};
  opacity: 1;
`;
const ClickableImage = styled.a`
  display: inherit;
`;
const Image = styled.img`
  width: 100%;
  height: 100%;
  opacity: 1;
`;
const BlurredImage = styled.img`
  width: 100%;
  height: 100%;
  opacity: 1;
`;
const ImageContainer = styled.div`
  margin: 3%;
  position: relative;
`;
const BlurredImageCaption = styled.div`
  display: flex;
  flex-direction: column;
  position: absolute;
  bottom: 10px;
  left: 10px;
`;
const Name = styled.span`
  font: normal normal bold 15px Raleway;
`;
const Institute = styled.div`
  width: 54px;
  height: 33px;
  margin-top: 20px;
  color: ${(props) => props.color};
  text-align: left;
  font: normal normal bold 24px Raleway;
  letter-spacing: 0px;
`;
const Year = styled.div`
  width: 61px;
  height: 33px;
  margin-bottom: 15px;
  text-align: left;
  font: italic normal normal 20px Raleway;
  letter-spacing: 0px;
  color: #000000;
`;
const Description = styled.text`
  text-align: left;
  font: normal normal normal 15px Raleway;
  color: #272727;
`;

export default Card;
