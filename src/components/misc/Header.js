import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { title } from "../../assets/texts/header";
import CNPQLogo from "../../assets/images/logo.png";

function Header() {
  const subtitleColor = "#272727";
  const fontFamily = "Raleway";
  const logoWidth = 155;
  const logoHeight = 46;

  return (
    <Container>
      <Text fontFamily={fontFamily}>
        <Link to="/" style={{ flexFlow: "column nowrap" }}>
          <FirstLine>{title[0]}</FirstLine>
          <SecondLine>{title[1]}</SecondLine>
          <ThirdLine>{title[2]}</ThirdLine>
        </Link>
      </Text>
      <Link style={{ alignItems: "start", display: "flex" }} to="/">
        <img width={logoWidth} height={logoHeight} src={CNPQLogo} alt="logo" />
      </Link>
    </Container>
  );
}

Header.defaultProps = {
  fontFamily: "sans-serif",
  subtitleColor: "#000000",
  titleColor: "#000000",
};

const Container = styled.div`
  display: flex;
  grid-column-start: 3;
  grid-row-start: 2;
  justify-content: space-between;
  overflow: hidden;
  width: 100%;
`;

const Text = styled.div`
  display: flex;
  font-family: ${(props) => props.fontFamily};
  height: fit-content;

  > a {
    align-items: flex;
    display: flex;
    height: 100%;
    text-decoration: none !important;
  }
`;

const FirstLine = styled.span`
  color: #272727;
  font-size: 40px;
  font-weight: 700;
  font-style: normal;
  font-stretch: normal;
`;

const SecondLine = styled.span`
  font-size: 25px;
  color: #272727;
`;

const ThirdLine = styled.span`
  color: #272727;
  font-size: 25px;
  font-weight: 700;
`;

export default Header;
