import FauLogo from "../images/fau.png";
import FauAberta from "../images/screenshots/Screenshot_FAU_Aberta_10-2022.png";
import ImeLogo from "../images/ime.png";
import ImeAberto from "../images/screenshots/Screenshot_IME_Aberto_10-2022.png";
import FearpLogo from "../images/fearp.png";
import FearpAberta from "../images/screenshots/Screenshot_FEARP_Aberta_10-2022.png";
import FdrpAberta from "../images/screenshots/Screenshot_FDRP_Aberta_12-2023.png";
import FdrpLogo from "../images/FDRP.png";
import BlurredPage from "../images/screenshots/Screenshot_CPQ_Aberta_Blurred_10-2022.png";
import FisicaLogo from "../images/fisica.png";
import FISICAAberta from "../images/screenshots/Screenshot_FISICA_Aberta_11_2023.png";
import IauAberto from "../images/screenshots//Screenshot_IAU_Aberto_12-2023.png";
import IauLogo from "../images/IAU.png";
import IoLogo from "../images/io.png";
import IoAberta from "../images/screenshots/io.png";
import FzeaLogo from "../images/fzea.png";
import FzeaAberta from "../images/screenshots/fzea.png";

const FAU = {
  nome: "Faculdade de Arquitetura e Urbanismo",
  sigla: "FAU",
  ano: "2020",
  descricao:
    "Criada para a faculdade de arquitetura e urbanismo da USP Butantã,  a FAUAberta foi a primeira da iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes, além de informações dos 3 departamentos - AUH, AUP e AUT.",
  cor: "#000000",
  logo: FauLogo,
  logoWidth: "138px",
  logoHeight: "60px",
  imagem: FauAberta,
  link: "http://cpqs-abertas-fau.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const FEARP = {
  nome: "Faculdade de Economia e Administração de Ribeirão Preto",
  sigla: "FEARP",
  ano: "2021",
  descricao:
    "Criada para a faculdade de economia e administração da USP Ribeirão Preto, a FEA-RPaberta foi a segunda da iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes, além de informações dos 3 departamentos - RAD, REC, RCC.",
  cor: "#428BCA",
  logo: FearpLogo,
  logoWidth: "80px",
  logoHeight: "60px",
  imagem: FearpAberta,
  link: "http://cpqs-abertas-fearp.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const IME = {
  nome: "Instituto de Matemática e Estatística",
  sigla: "IME",
  ano: "2022",
  descricao:
    "Criado para o Instituto de Matemática e Estatística da USP Butantã, o IMEaberto foi a terceira da iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes, além de informações dos 4 departamentos - MAT, MAP, MAE, DCC.",
  cor: "#9D0A0E",
  logo: ImeLogo,
  logoWidth: "160px",
  logoHeight: "60px",
  imagem: ImeAberto,
  link: "http://cpqs-abertas-ime.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const FISICA = {
  nome: "Instituto de Física",
  sigla: "IF",
  ano: "2023",
  descricao:
    "Criado para o Instituto de Física da USP Butantã, a FISICAaberta foi a quarta da iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes, além de informações dos 6 departamentos - FAP, FMT, FEP, FGE, FMA e FNC",
  cor: "#185C8C",
  logo: FisicaLogo,
  imagem: FISICAAberta,
  logoWidth: "80px",
  logoHeight: "60px",
  link: "http://cpqs-abertas-fisica.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const FDRP = {
  nome: "Faculdade de Direito de Ribeirão Preto",
  sigla: "FDRP",
  ano: "2023",
  descricao:
    "Criado para a Faculdade de Direito de Ribeirão Preto, a FDRPaberta foi a quinta da iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes, além de informações dos 3 departamentos - DPP, DDP e DFB",
  cor: "#971B2F",
  logo: FdrpLogo,
  imagem: FdrpAberta,
  logoWidth: "80px",
  logoHeight: "60px",
  link: "http://cpqs-abertas-fdrp.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const IAU = {
  nome: "Instituto de Arquitetura e Urbanismo",
  sigla: "IAU",
  ano: "2023",
  descricao:
    "Criado para o Instituto de Arquitetura e Urbanismo de São Carlos, o IAUaberto foi a sexta da iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes",
  cor: "#145599",
  logo: IauLogo,
  imagem: IauAberto,
  logoWidth: "80px",
  logoHeight: "60px",
  link: "http://cpqs-abertas-iau.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const IO = {
  nome: "Instituto de Oceanografia",
  sigla: "IO",
  ano: "2024",
  descricao:
    "Criado para o Instituto de Oceanografia da USP Butantã, o IOaberto foi a sétima iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes, além de informações dos 2 departamentos - DOB e DOF",
  cor: "#011631",
  logo: IoLogo,
  imagem: IoAberta,
  logoWidth: "80px",
  logoHeight: "60px",
  link: "http://cpqs-abertas-io.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const FZEA = {
  nome: "Faculdade de Zootecnia e Engenharia de Alimentos",
  sigla: "FZEA",
  ano: "2024",
  descricao:
    "Criado para o Faculdade de Zootecnia e Engenharia de Alimentos da USP Pirassununga, a FZEAaberta foi a oitava iniciativa das CPQS abertas. O site contém informações gerais do instituto e dos docentes, além de informações dos 5 departamentos - ZAB, ZAZ, ZEA, ZEB e ZMV",
  cor: "#1094AB",
  logo: FzeaLogo,
  imagem: FzeaAberta,
  logoWidth: "80px",
  logoHeight: "60px",
  link: "http://cpqs-abertas-fzea.s3-website-sa-east-1.amazonaws.com/",
  disponivel: true,
};

const cpqs = [FAU, FEARP, IME, FISICA, FDRP, IAU, IO, FZEA];

export { cpqs };
